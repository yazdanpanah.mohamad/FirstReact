import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
//import {Route, Router, IndexRoute, hashHistory} from 'react-router';

import TodoApp from './components/TodoApp';
import actions from './actions/actions';
import store from './store/configureStore';
import TodoAPI from './api/TodoAPI';



import registerServiceWorker from './registerServiceWorker';


import './assets/bootstrap/css/bootstrap.min.css';
import './styles/sass/app.scss';



store.subscribe(() => {
    var state = store.getState();
    console.log('New state', state);
    TodoAPI.setTodos(state.todos);
});

var initialTodos = TodoAPI.getTodos();
store.dispatch(actions.addTodos(initialTodos));



ReactDOM.render(
    <Provider store={store}>
        <TodoApp/>
    </Provider>,
    document.getElementById('app')
);


registerServiceWorker();
