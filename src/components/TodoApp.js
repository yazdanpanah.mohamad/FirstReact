import React, { Component } from 'react';
import uuid from 'node-uuid';
import moment from 'moment';


// components
import TodoList from './TodoList'
import AddTodo from './AddTodo';
import TodoSearch from './TodoSearch';

class TodoApp extends Component {
  render() {
      return (
          <div className="container">
              <h1 className="page-title">Todo App</h1>
              <div className="row">
                  <div className="col-xs-12 ">
                      <div className="centered">
                          <TodoSearch/>
                          <TodoList/>
                          <AddTodo/>
                      </div>
                  </div>
              </div>
          </div>
      );
  }
}

export default TodoApp;
